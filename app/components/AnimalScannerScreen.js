import React, {
  View,
  StyleSheet,
  AsyncStorage
} from 'react-native';

var tts = require('react-native-android-speech');

import Popup from 'react-native-popup';
import Spinner from 'react-native-loading-spinner-overlay';
import Camera from 'react-native-camera';
import CustomButton from './CustomButton';

var FileTransfer = require('react-native-file-transfer-android');

export default class CaptureScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = { 
      spinnerVisible: false,
      timeoutId: -1
    };
  } 

  render() {
    return (
      <View style={styles.container}>
        <Camera style={styles.camera}
          ref={(cam) => { this.cam = cam; }}
          captureTarget={Camera.constants.CaptureTarget.disk}>
        </Camera>
        <CustomButton 
          style={styles.takePictureButton}
          textStyle={styles.takePictureButtonText}
          customText='Analyze'
          onPress={this._takePicture.bind(this)} />
        <View style={styles.rectangle}></View>
        <Popup ref={(popup) => { this.popup = popup }} />
        <Spinner visible={this.state.spinnerVisible} />
      </View>
    );
  }

  _takePicture() {
    tts.speak({
      text: 'Analyzing. Please wait for a while.',
      pitch: 1.5,
      forceStop: true,
      language: 'en'
    }).then((isSpeaking) => {
      console.log('Is Speaking:', isSpeaking);
    }).catch((err) => {
      console.log(err);
    });

    this.cam.capture()
      .then((imageUri) => {
        this._uploadPictureToServerAndReceiveAnimalProfile.call(this, imageUri);
      })
      .catch((err) => {
        this.popup.alert('Can\'t take picture. Try again!');
      });
  }

  _uploadPictureToServerAndReceiveAnimalProfile(imageUri) {
    FileTransfer.upload({
      uri: imageUri,
      // uri: '/storage/emulated/0/rncamera/animal.jpg',
      uploadUrl: 'https://anidex.herokuapp.com/api/animals/info',
      fileName: 'animal.jpg',
      mimeType: 'image/jpg',
      headers: {
        'Accept': 'application/json'
      },
      data: {}
    }, this._onReceiveResponse.call(this, imageUri));
    var self = this;
    this.setState({ 
      spinnerVisible: true,
      timeoutId: setTimeout(self._displayNetworkErrorPopup.bind(self, imageUri), 15000)
    });
  }

  _onReceiveResponse(imageUri) {
    return (err, res) => {
      clearTimeout(this.state.timeoutId);
      this.setState({ spinnerVisible: false, timeoutId: -1 });
      if (err) { 
        console.log(err);
        this._displayNetworkErrorPopup.call(this, imageUri);
      } else {
        try {
          res = JSON.parse(res);
          if (Object.keys(res).length > 0) {
            res['imageUri'] = imageUri;
            // res['imageUri'] = 'file:///storage/emulated/0/rncamera/animal.jpg';
            var currentDate = new Date();
            res['scannedDateTime'] = currentDate.toDateString() + ',\n' + currentDate.toLocaleTimeString();
            res['givenName'] = '';
            res['isNew'] = true;
            this.props.onReceiveAnimalProfile(res);
          } else {
            this.popup.tip({
              title: 'Unknown Animal',
              content: 'We can\'t identify the animal!'
            });
          }
        } catch (err) {}
      }
    };
  }

  _displayNetworkErrorPopup(imageUri) {
    clearTimeout(this.state.timeoutId);
    this.setState({ spinnerVisible: false, timeoutId: -1 });
    this.popup.confirm({
      title: 'Network Error',
      content: 'An error has occurred.',
      ok: {
        text: 'Retry',
        callback: () => {
          this._uploadPictureToServerAndReceiveAnimalProfile(imageUri);
        }
      },
      cancel: {
        text: 'Cancel',
        callback: () => {}
      }
    });
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EDE9E6',
    paddingTop: 55
  },
  camera: {
    flex: 5
  },
  takePictureButton: {
    flex: 0.5,
    backgroundColor: '#545257',
    justifyContent: 'center',
    alignItems: 'center'
  },
  takePictureButtonText: {
    color: '#EEE',
    fontSize: 16,
    marginVertical: 10
  },
  rectangle: {
    position: 'absolute',
    width: 300,  
    height: 200,
    top: 200,
    left: 33,
    backgroundColor: 'rgba(255, 255, 255, 0.1)',
    borderColor: 'rgba(255, 0, 0, 0.4)',
    borderTopWidth: 2,
    borderBottomWidth: 2
  }
});
