import React, {
  Text,
  TouchableOpacity,
  Image
} from 'react-native';

export default class CustomButton extends React.Component {
  render() { 
    var customContent = this.props.imageSource ? (
        <Image 
          style={this.props.imageStyle}
          source={this.props.imageSource}/>
      ) : (
        <Text
          style={this.props.textStyle}>
          {this.props.customText}
        </Text>
      );

    return (
      <TouchableOpacity
        style={this.props.style}
        onPress={this.props.onPress}>
        {customContent}
      </TouchableOpacity>
    );
  }
}
