import React, {
  View,
  StyleSheet,
  Text,
  TouchableHighlight,
  Image
} from 'react-native';

import CustomButton from './CustomButton';

export default class RecordScreen extends React.Component {
  _getAllAnimals() {
    var recent = [];
    var currentSize = 0;
    var maxSize = 3;
    for (var givenName in this.props.animals) {
      if (currentSize < maxSize) {
        var animal = this.props.animals[givenName];
        this._addAnimalToList(recent, animal);
        currentSize += 1;
      } else {
        break;
      }
    }
    return recent;
  }

  _addAnimalToList(animalList, animal) {
    animalList.push(
      <TouchableHighlight
        key={animal.givenName}
        onPress={() => {
          this.props.onSelectAnimal(animal);
        }}
        underlayColor='#DEE6B5'>
        <View style={styles.detailRow}>
          <Image 
            style={styles.animalIcon}
            source={{ uri: animal.imageUri }} />
          <Text style={styles.recordDetailsText}>
            {animal.givenName}
          </Text>
          <Text style={styles.animalName}>
            ({animal.name})
          </Text>
        </View>
      </TouchableHighlight>
    );
  }

  _getAnimalNameWithHighestEncounterRate() {
    var animalNameCount = {};
    for (var givenName in this.props.animals) {
      var name = this.props.animals[givenName].name;
      var currentCount = animalNameCount[name];
      animalNameCount[name] = currentCount ? currentCount += 1 : 0;
    }
    var max = '';
    for (var name in animalNameCount) {
      var count = animalNameCount[name];
      if (max === '') {
        max = name;
      } else {
        if (count > animalNameCount[max]) {
          max = name;
        }
      }
    }
    return max.toUpperCase();
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.recordDetailsTitle}>
          Recent Encountered Animals
        </Text>
        <View style={styles.recordDetailsContainer}>
          {this._getAllAnimals()}
        </View>
        <Text style={styles.recordDetailsTitle}>
          Other Encounter Details
        </Text>
        <View style={styles.recordDetailsContainer}>
          <View style={styles.detailRow}>
            <Text style={styles.recordDetailsText}>
              {'Total encountered animals: ' + Object.keys(this.props.animals).length}
            </Text>
          </View>
          <View style={styles.detailRow}>
            <Text style={styles.recordDetailsText}>
              {'Highest encounter rate: ' + this._getAnimalNameWithHighestEncounterRate()}
            </Text>
          </View>         
        </View>
        <CustomButton 
          onPress={() => {
            this.props.onScanButtonPress();
          }}
          style={styles.scanButton}
          textStyle={styles.scanButtonText}
          customText='Scan Now'/>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 55,
    backgroundColor: '#EDE9E6',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  recordDetailsTitle: {
    color: 'grey',
    fontSize: 16,
    fontWeight: '500',
    marginTop: 10,
    marginBottom: 10
  },
  recordDetailsContainer: {
    backgroundColor: 'rgba(222, 230, 181, 0.3)',
    borderColor: '#F2B885',
    borderRadius: 5,
    borderWidth: 1,
    width: 300
  },
  scanButton: {
    backgroundColor: '#545257',
    borderColor: '#B4AAB5',
    borderWidth: 1,
    borderRadius: 3,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50,
    paddingLeft: 6,
    paddingRight: 6,
    paddingTop: 4,
    paddingBottom: 4
  },
  scanButtonText: {
    color: 'white',
    fontSize: 16,
    fontWeight: '100',
    textAlign: 'center'
  },
  detailRow: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 5
  },
  animalIcon: {
    width: 20,
    height: 20,
    borderRadius: 10,
    borderColor: '#F2B885',
    borderWidth: 1
  },
  recordDetailsText: {
    color: 'black',
    fontSize: 16,
    fontWeight: '100',
    marginLeft: 20
  },
  animalName: {
    color: 'grey',
    fontSize: 12,
    fontWeight: '100',
    marginLeft: 5
  }
});