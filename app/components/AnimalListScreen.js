import React, {
	StyleSheet,
	View,
	ScrollView,
	Text,
	Image,
	TouchableHighlight
} from 'react-native';

export default class AnimalListScreen extends React.Component {
	_getAllAnimals() {
		var animalList = [];
		for (var givenName in this.props.animals) {
			var animal = this.props.animals[givenName];
			this._addAnimalToList(animalList, animal);
		}
		return animalList;
	}

	_addAnimalToList(animalList, animal) {
		animalList.push(
			<TouchableHighlight
				key={animal.givenName}
				onPress={() => {
					this.props.onSelectAnimal(animal);
				}}
			  underlayColor='#DEE6B5'>
				<View style={styles.detailRow}>
				  <Image 
				    style={styles.animalIcon}
				    source={{ uri: animal.imageUri }} />
				  <Text style={styles.animalGivenName}>
				  	{animal.givenName}
				  </Text>
				  <Text style={styles.animalName}>
				    ({animal.name})
				  </Text>
				</View>
			</TouchableHighlight>
		);
	}

	render() {
		return (
			<View style={styles.container}>
			  <ScrollView
			    contentContainerStyle={styles.scrollView}>
			    <View style={styles.animalList}>
			      {this._getAllAnimals()}
			    </View>
			  </ScrollView>
			</View>
		);
	}
}

var styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#EDE9E6',
		marginTop: 55
	},
	scrollView: {
		alignItems: 'stretch'
	},
	animalList: {
		flex: 1
	},
	detailRow: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'flex-start',
		paddingTop: 5,
		paddingBottom: 5,
		paddingLeft: 5,
		borderBottomColor: '#B4AAB5',
		borderBottomWidth: 1
	},
	animalIcon: {
		width: 40,
		height: 40,
    borderRadius: 10,
    borderColor: '#F2B885',
    borderWidth: 1
	},
	animalGivenName: {
		color: 'black',
		fontSize: 16,
		fontWeight: '100',
		marginLeft: 20
	},
	animalName: {
		color: 'grey',
		fontSize: 12,
		fontWeight: '100',
		marginLeft: 5
	}
});