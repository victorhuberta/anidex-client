import React, {
  View,
  Text,
  Image,
  StyleSheet,
  TextInput,
  ScrollView
} from 'react-native';

import CustomButton from './CustomButton';

export default class AnimalProfileScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = { animal: this.props.animal };
  } 

  _updateState(givenName) {
    var animalClone = Object.assign({}, this.state.animal);
    animalClone.givenName = givenName;
    this.setState({ animal: animalClone });
    return animalClone;
  }

  _displayAnimalGivenName() {
    if (this.state.animal.isNew) {
      return (
        <TextInput 
          ref='givenName'
          autoFocus={true}
          style={styles.animalGivenName}
          autoCapitalize='words'
          placeholder='Give it a name!'
          value={this.state.animal.givenName}
          onChangeText={(givenName) => {
            this._updateState(givenName);
          }} />
      );
    } else {
      return (
        <View style={{ height: 40 }}>
          <Text style={[styles.animalGivenName, { marginTop: 10 }]}>
            {this.state.animal.givenName}
          </Text>
        </View>
      );
    }
  }

  _displayButtonsContainer() {
    if (this.state.animal.isNew) {
      return (
        <View style={styles.buttonsContainer}>
          <CustomButton 
            style={styles.button}
            textStyle={styles.buttonText}
            customText='Save'
            onPress={() => {
              this._saveAnimalProfile(this.state.animal.givenName);
            }} />
          <CustomButton 
            style={[styles.button, { marginLeft: 5 }]}
            textStyle={styles.buttonText}
            customText='Discard'
            onPress={() => {
              this._discardAnimalProfile();
            }} />
        </View>
      );
    } else {
      return null;
    }
  }

  _saveAnimalProfile(givenName) {
    var animalClone = this._updateState(givenName);
    this.props.onSaveAnimalProfile(animalClone);
  }

  _discardAnimalProfile() {
    this.props.onDiscardAnimalProfile();
  }

  render() {
    return (
      <View style={styles.container}>
        {this._displayAnimalGivenName()}
        <View style={styles.profileContainer}>
          <View style={styles.imageAndScanDetailsContainer}>
            <Image
              style={styles.image}
              source={{ uri: this.props.animal.imageUri }} />
            <View style={styles.scanDetailsContainer}>
              <Text style={styles.scanDetailsText}>
                {'Encountered On:\n\n' + this.props.animal.scannedDateTime}
              </Text>
              {this._displayButtonsContainer()}
            </View>
          </View>
          <View style={styles.detailsContainer}>
            <ScrollView 
              contentContainerStyle={styles.scrollView} >
              <View style={styles.profileFieldsAndValues}>
                {this._displayAllProfileFields()}
              </View>
            </ScrollView>
          </View>
        </View>
      </View>
    );
  }

  _displayAllProfileFields() {
    var detailRows = [];
    for (var key in this.props.animal.profile) {
      var value = this.props.animal.profile[key];
      detailRows.push(
        <View key={detailRows.length} style={styles.detailRow}>
          <Text style={styles.detailRowKey}>
            {key}
          </Text>
          <View style={styles.detailRowValueContainer}>
            <Text style={styles.detailRowValue}>
              {value}
            </Text>
          </View>
        </View>
      );
    }
    return detailRows;
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EDE9E6',
    marginTop: 55,
    alignItems: 'stretch'
  },
  profileContainer: {
    flex: 4,
    alignItems: 'stretch'
  },
  imageAndScanDetailsContainer: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    backgroundColor: '#B4AAB5',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10
  },
  scanDetailsContainer: {
    flex: 1,
    alignItems: 'center',
    paddingLeft: 5,
    paddingTop: 10
  },
  scanDetailsText: {
    color: 'black',
    fontSize: 14,
    fontWeight: '100'
  },
  image: {
    width: 180,
    height: 180,
    borderRadius: 10,
    borderColor: '#F2B885',
    borderWidth: 1
  },
  buttonsContainer: {
    flexDirection: 'row'
  },
  button: {
    backgroundColor: '#545257',
    borderColor: '#B4AAB5',
    borderWidth: 1,
    borderRadius: 3,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 6,
    paddingRight: 6,
    paddingTop: 4,
    paddingBottom: 4,
    marginTop: 50
  },
  buttonText: {
    color: 'white',
    fontSize: 16,
    fontWeight: '100',
    textAlign: 'center'
  },
  animalGivenName: {
    height: 40,
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  detailsContainer: {
    flex: 1,
    alignItems: 'stretch',
  },
  scrollView: {
    alignItems: 'center'
  },
  profileFieldsAndValues: {
    flex: 1
  },
  detailRow: {
    width: 300,
    paddingTop: 8,
    paddingBottom: 8
  },
  detailRowKey: {
    color: 'black',
    fontSize: 16,
    fontWeight: '100',
    textAlign: 'center'
  },
  detailRowValueContainer: {
    borderColor: 'lightblue',
    borderWidth: 1,
    borderRadius: 3,
    backgroundColor: '#DEE6B5',
    justifyContent: 'center',
    alignItems: 'center'
  },
  detailRowValue: {
    color: 'black',
    fontSize: 20,
    fontWeight: '100'
  }
});