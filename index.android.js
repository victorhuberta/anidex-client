/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';
import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  Navigator,
  AsyncStorage
} from 'react-native';

import Popup from 'react-native-popup';

import CustomButton from './app/components/CustomButton';
import RecordScreen from './app/components/RecordScreen';
import AnimalScannerScreen from './app/components/AnimalScannerScreen';
import AnimalProfileScreen from './app/components/AnimalProfileScreen';
import AnimalListScreen from './app/components/AnimalListScreen';

class anidex extends Component {
  constructor(props) {
    super(props);
    this.state = { animals: {} };
    this._loadAnimalProfiles();
  }

  async _saveAnimalProfiles(animals) {
    try {
      await AsyncStorage.setItem('@Anidex:animals', JSON.stringify(animals));
    } catch (err) {
      console.log('AsyncStorage Error:', err);
    }
  }

  async _loadAnimalProfiles() {
    try {
      var animals = await AsyncStorage.getItem('@Anidex:animals');
      if (animals) {
        this.setState({ animals: JSON.parse(animals) });
      }
    } catch (err) {
      console.log('AsyncStorage Error:', err);
    }
  }

  _checkValidityOfProfileThenSave(navigator, animal) {
    if (animal.givenName.length > 0) {
      if (this.state.animals[animal.givenName] === undefined) {
        navigator.push({
          name: 'record', animal
        });
        this._addAnimalProfileAndSave(animal);
      } else {
        this.popup.tip({
          title: 'A Same Name Exists!',
          content: 'Please choose a different name.'
        });
      }
    } else {
      this.popup.tip({
        title: 'Before You Save...',
        content: 'Make sure you give it a name!'
      });
    }
  }

  _addAnimalProfileAndSave(animal) {
    var animalsClone = Object.assign({}, this.state.animals);
    animal.isNew = false;
    animalsClone[animal.givenName] = animal;
    this.setState({ animals: animalsClone });
    this._saveAnimalProfiles(this.state.animals);
  }

  _removeAnimalFromListAndSave(animal) {
    var animalsClone = Object.assign({}, this.state.animals);
    delete animalsClone[animal.givenName];
    this.setState({ animals: animalsClone });
    this._saveAnimalProfiles(this.state.animals);
  }

  renderScene(route, navigator) {
    switch (route.name) {
      case 'record':
        return (
          <RecordScreen
            animals={this.state.animals}
            onScanButtonPress={() => {
              navigator.pop();
            }}
            onSelectAnimal={(animal) => {
              navigator.push({
                name: 'animalProfile', 
                animal,
                onDeleteAnimal: () => {
                  this._removeAnimalFromListAndSave(animal);
                }
              });
            }} />
        );
      case 'animalScanner':
        return (
          <AnimalScannerScreen
            spinnerVisible={false}
            onReceiveAnimalProfile={(animal) => {
              if (Object.keys(animal).length > 0) {
                navigator.push({
                  name: 'animalProfile', 
                  animal,
                  onDeleteAnimal: () => {
                    this._removeAnimalFromListAndSave(animal);
                  }
                });
              }
            }} />
        );
      case 'animalProfile':
        return (
          <View style={{ flex: 1 }}>
            <AnimalProfileScreen
              animal={route.animal}
              onSaveAnimalProfile={(animal) => {
                this._checkValidityOfProfileThenSave(navigator, animal);
              }}
              onDiscardAnimalProfile={() => {
                navigator.pop();
              }} />
              <Popup ref={(popup) => { this.popup = popup }} />
          </View>
        );
      case 'animalList':
        return (
          <AnimalListScreen
            animals={this.state.animals}
            onSelectAnimal={(animal) => {
              navigator.push({
                name: 'animalProfile', 
                animal,
                onDeleteAnimal: () => {
                  this._removeAnimalFromListAndSave(animal);
                }
              });
            }} />
        );
      default:
        return null;
    }
  }

  render() {
    return (
      <Navigator 
        style={styles.navContainer}
        initialRoute={{ name: 'animalScanner' }}
        renderScene={this.renderScene.bind(this)}
        navigationBar={
          <Navigator.NavigationBar 
            routeMapper={NavBarRouteMapper}
            style={styles.navBar} />
        } />
    );
  }
}

var NavBarRouteMapper = {
  LeftButton: function (route, navigator, index, navState) {
    switch (route.name) {
      case 'animalScanner':
        return (
          <CustomButton 
            style={styles.navBarLeftButton}
            textStyle={styles.navBarButtonText}
            customText='Animals'
            onPress={() => {
              navigator.push({
                name: 'animalList'
              });
            }} />  
        );
      default:
        return (
          <CustomButton 
            style={styles.navBarLeftButton}
            textStyle={styles.navBarButtonText}
            customText='Back'
            onPress={() => {
              navigator.pop();
            }} />
        );
    }
  },
  Title: function (route, navigator, index, navState) {
    switch (route.name) {
      case 'record':
        return (
          <View style={styles.navBarTitleContainer}>
            <Text style={styles.navBarTitleText}>
              User Record
            </Text>
          </View>
        );
      case 'animalScanner':
        return (
          <View style={styles.navBarTitleContainer}>
            <Text style={styles.navBarTitleText}>
              Animal Scanner
            </Text>
          </View>
        );
      case 'animalProfile':
        return (
          <View style={styles.navBarTitleContainer}>
            <Text style={styles.navBarTitleText}>
              Profile: {route.animal.name ? route.animal.name.toUpperCase() : 'UNKNOWN'}
            </Text>
          </View>
        );
      case 'animalList':
        return (
          <View style={styles.navBarTitleContainer}>
            <Text style={styles.navBarTitleText}>
              Encountered Animals
            </Text>
          </View>
        );       
      default:
        return null;
    }
  },
  RightButton: function (route, navigator, index, navState) {
    switch (route.name) {
      case 'animalScanner':
        return (
          <CustomButton 
            style={styles.navBarRightButton}
            textStyle={styles.navBarButtonText}
            customText='Record'
            onPress={() => {
              navigator.push({
                name: 'record'
              });
            }} />        
        );
      case 'animalProfile':
        if (!route.animal.isNew) {
          return (
            <CustomButton 
              style={styles.navBarRightButton}
              textStyle={styles.navBarButtonText}
              customText='Delete'
              onPress={() => {
                navigator.pop();
                route.onDeleteAnimal();
              }} />  
          );
        } else {
          return null;
        }
      default:
        return null;
    }
  }
};

const styles = StyleSheet.create({
  navContainer: {
    flex: 1
  },
  navBar: {
    backgroundColor: '#545257',
    borderBottomColor: '#B4AAB5',
    borderBottomWidth: 1
  },
  navBarTitleContainer: {
    flex: 1,
    marginVertical: 10,
    marginLeft: 30,
    width: 160,
    justifyContent: 'center',
    alignItems: 'center'
  },
  navBarTitleText: {
    color: 'white',
    fontSize: 16,
    fontWeight: '500'
  },
  navBarLeftButton: {
    marginTop: 5,
    marginLeft: 10
  },
  navBarRightButton: {
    marginTop: 5,
    marginRight: 10
  },
  navBarButtonText: {
    color: '#EEE',
    fontSize: 16,
    marginVertical: 10
  }
});

AppRegistry.registerComponent('anidex', () => anidex);
